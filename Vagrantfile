# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = '2'

require 'yaml'

module OS
    def OS.windows?
        (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
    end

    def OS.mac?
        (/darwin/ =~ RUBY_PLATFORM) != nil
    end

    def OS.unix?
        !OS.windows?
    end

    def OS.linux?
        OS.unix? and not OS.mac?
    end
end

if OS.windows?
    host_os = "windows"
elsif OS.mac?
    host_os = "mac"
elsif OS.unix?
    host_os = "unix"
elsif OS.linux?
    host_os = "linux"
else
    host_os = "unknown platform."
end

current_dir=File.dirname(File.expand_path(__FILE__))
params = YAML::load_file("#{current_dir}/config.yaml")


Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = params['box_os']
  config.vm.hostname=params['box_hostname']
  #config.vm.box_version = params['box_version']
  config.vm.box_url = params['box_url']
  config.vm.network "forwarded_port", guest: params['box_client_http_port'],    host: params['box_host_http_port'], auto_correct: true
  config.vm.network "forwarded_port", guest: params['box_client_mysql_port'],  host: params['box_host_mysql_port'], auto_correct: true
  config.vm.network "forwarded_port", guest: params['box_client_mongo_port'], host: params['box_host_mongo_port'], auto_correct: true
  config.vm.synced_folder params['project_root_dir_host'], params['project_root_dir_guest'] ,create:true ,group:"www-data" ,owner:"vagrant" ,:mount_options => ["dmode=777", "fmode=777"]
  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"
  config.vm.provision "shell" do |s|
    s.privileged = false
    s.path = "provision/setup.sh"
    s.args = [
      host_os,
      params['box_name'],
      params['box_hostname'],
      params['box_title'],
      params['github_token'],
      params['project_root_dir_guest']
    ]
  end
  config.ssh.forward_agent = true
  config.vm.provider "virtualbox" do |vb|
   vb.name = params['box_name']
   vb.cpus = params['box_cpu']
   vb.memory = params['box_memory']
   opts = ['modifyvm', :id, '--natdnshostresolver1', 'on']
   vb.customize opts
  end
end