#!/bin/bash
dir=$(pwd);

if [[ $dir == *"vendor/bin"* ]]; then
        executable="../intaglio/nuclio-core/bin/atomos";
else
        executable="vendor/intaglio/nuclio-core/bin/atomos";
fi

touch .hhconfig;

$executable "$@";