#!/bin/bash

host_os=$1;

echo 'Creating /srv structure'
sudo mkdir -p /srv/www/nuclio.dev.lan
sudo chown -R vagrant:www-data /srv/www

#echo "Configuring nginx"
#sudo cp /vagrant/provision/nuclio.dev.lan.conf /etc/nginx/sites-available/nuclio.dev.lan.conf
#cd /etc/nginx/sites-enabled
#sudo ln -s ../sites-available/nuclio.dev.lan.conf

cd /srv/www/nuclio.dev.lan

#alias composer='hhvm /usr/local/bin/composer'
hhvm /usr/local/bin/composer require intaglio/nuclio-standard
hhvm /usr/local/bin/composer require facebook/xhp-lib

hhvm /usr/local/bin/composer update
#mkdir ./hello
#mkdir ./public
#cp -R /vagrant/provision/public/. ./public
touch .hhconfig
cd ~
rm ~/.hhvm.hhbc

if [[ host_os="windows" ]]; then
	echo "Fixing composer bins for NTFS..."
	sudo rm /srv/www/nuclio.dev.lan/vendor/bin/nuclio
	sudo rm /srv/www/nuclio.dev.lan/vendor/bin/atomos
	sudo cp /vagrant/provision/nuclio_bin.sh /srv/www/nuclio.dev.lan/vendor/bin/nuclio
	sudo cp /vagrant/provision/atomos_bin.sh /srv/www/nuclio.dev.lan/vendor/bin/atomos
	sed -i -e 's/\r$//' /srv/www/nuclio.dev.lan/vendor/bin/nuclio
	sed -i -e 's/\r$//' /srv/www/nuclio.dev.lan/vendor/bin/atomos
fi

echo "Configuring virtual host..."
sudo cp /vagrant/provision/hhvm_server.ini /etc/hhvm/server.ini

echo "Setting up and configuring Nuclio..."
cd /srv/www/nuclio.dev.lan

if [[ host_os="windows" ]]; then
	vendor/bin/nuclio setup --config ./sampleApp/config --user vagrant --group vagrant
	vendor/bin/atomos make::app --name sampleApp --sample
else
	hhvm vendor/bin/nuclio setup --config ./sampleApp/config --user vagrant --group vagrant
	hhvm vendor/bin/atomos make::app --name sampleApp --sample
fi

sudo run-parts /etc/update-motd.d/ >> /dev/null

sudo service hhvm restart
