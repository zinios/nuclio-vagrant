#!/bin/bash
dir=$(pwd);

if [[ $dir == *"vendor/bin"* ]]; then
        executable="../intaglio/nuclio-core/bin/nuclio";
else
        executable="vendor/intaglio/nuclio-core/bin/nuclio";
fi

touch .hhconfig;

$executable "$@";